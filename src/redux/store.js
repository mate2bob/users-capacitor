import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import usersReducer from './reducer';

const rootReducer = combineReducers({
  users: usersReducer,
});

const configureStore = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default configureStore;
