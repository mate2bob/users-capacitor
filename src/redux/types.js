export const SET_USERS_LIST = 'SET_USERS_LIST';
export const NEXT_PAGE = 'NEXT_PAGE';
export const PREVIOUS_PAGE = 'PREVIOUS_PAGE';
export const FILTER_LIST = 'FILTER_LIST';
export const TOGGLE_LOADING = 'TOGGLE_LOADING';
