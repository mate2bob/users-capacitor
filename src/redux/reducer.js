import { SET_USERS_LIST, NEXT_PAGE, PREVIOUS_PAGE, FILTER_LIST, TOGGLE_LOADING } from './types';

const initialState = {
  list: [],
  filteredList: [],
  usersPerPage: 2,
  currentPage: 0,
  totalPage: 0,
  filter: '',
  isLoading: true,
};

function reducer(state = initialState, action) {
  const newState = { ...state };
  switch (action.type) {
    case SET_USERS_LIST:
      newState.list = action.payload.list;
      newState.filteredList = action.payload.list;
      newState.totalPage = Math.round(action.payload.list.length / newState.usersPerPage);
      break;
    case NEXT_PAGE:
      if (newState.currentPage + 1 <= newState.totalPage) {
        newState.currentPage += 1;
      }
      break;
    case PREVIOUS_PAGE:
      if (newState.currentPage - 1 >= 0) {
        newState.currentPage -= 1;
      }
      break;
    case FILTER_LIST: {
      newState.currentPage = 0;
      newState.filter = action.payload.filter;
      const filteredUsersList = newState.list.filter((user) => {
        const emailFilter = user.email.toLowerCase().includes(action.payload.filter.toLowerCase());
        const nameFilter = user.name.toLowerCase().includes(action.payload.filter.toLowerCase());
        return emailFilter || nameFilter;
      });
      newState.filteredList = filteredUsersList;
      newState.totalPage = Math.round(newState.filteredList.length / newState.usersPerPage);
      break;
    }
    case TOGGLE_LOADING:
      newState.isLoading = !newState.isLoading;
      break;
    default:
      break;
  }
  return newState;
}

export default reducer;
