import { SET_USERS_LIST, NEXT_PAGE, PREVIOUS_PAGE, FILTER_LIST, TOGGLE_LOADING } from './types';

export const setUsersList = (list) => {
  const action = {
    type: SET_USERS_LIST,
    payload: { list },
  };
  return action;
};

export const goToNextPage = () => {
  const action = {
    type: NEXT_PAGE,
  };
  return action;
};

export const goToPreviousPage = () => {
  const action = {
    type: PREVIOUS_PAGE,
  };
  return action;
};

export const filterList = (filter) => {
  const action = {
    type: FILTER_LIST,
    payload: { filter },
  };
  return action;
};

export const toggleLoading = () => {
  const action = {
    type: TOGGLE_LOADING,
  };
  return action;
};
