import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { getUsers } from './lib/api';
import UserCard from './components/UserCard';
import NavigationBar from './components/NavigationBar';
import { setUsersList, filterList, toggleLoading } from './redux/action';

function App() {
  const {
    list: usersList,
    filteredList,
    usersPerPage,
    currentPage,
    filter,
    isLoading,
  } = useSelector((state) => state.users);
  const dispatch = useDispatch();

  async function fetchUsersList() {
    await getUsers()
      .then(
        ({
          data: {
            data: { users },
          },
        }) => {
          dispatch(setUsersList(users));
          dispatch(toggleLoading());
        }
      )
      .catch(() => {
        console.error('Oh no, something bad happened!');
      });
  }

  useEffect(() => {
    fetchUsersList();
  }, []);

  const handleFiltering = ({ target: { value } }) => {
    if (value) {
      dispatch(filterList(value));
    }
  };

  const clearFilter = () => {
    dispatch(filterList(''));
  };

  const startFrom = currentPage * usersPerPage;
  const stopAt = currentPage * usersPerPage + usersPerPage;
  const displayList = filteredList.slice(startFrom, stopAt);

  return (
    <div>
      <h1>Ilume Users List {isLoading ? '' : `(${usersList.length})`}</h1>
      {isLoading ? (
        <div className="loadingContainer">Loading ! Please wait.</div>
      ) : (
        <>
          <div className="searchBar">
            <label htmlFor="search">Filter user by email or name :</label>
            <input
              type="text"
              value={filter}
              onChange={handleFiltering}
              placeholder="Type to filter results..."
              id="search"
              name="search"
            />
            <button onClick={() => clearFilter()} disabled={filter === ''} type="button">
              Clear
            </button>
          </div>
          <NavigationBar />
          <div className="usersListGrid">
            {displayList.length === 0 ? <div>No results found</div> : null}
            {displayList.map((user) => (
              <UserCard user={user} key={user.email} />
            ))}
          </div>
          <NavigationBar />
        </>
      )}
    </div>
  );
}

App.propTypes = {};

export default App;
