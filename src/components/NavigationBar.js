import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { goToNextPage, goToPreviousPage } from '../redux/action';

function NavigationBar() {
  const { currentPage, totalPage } = useSelector((state) => state.users);
  const dispatch = useDispatch();

  return (
    <div className="navigationBar">
      <div>
        <button
          onClick={() => dispatch(goToPreviousPage())}
          disabled={currentPage === 0}
          title={currentPage === 0 ? 'You are on the first page' : `Page ${currentPage}`}
          type="button"
        >
          Previous
        </button>
      </div>
      <div>
        Page {currentPage + 1}/{totalPage > 0 ? totalPage : 1}
      </div>
      <div>
        <button
          onClick={() => dispatch(goToNextPage())}
          disabled={currentPage === totalPage - 1}
          title={
            currentPage === totalPage - 1 ? 'You reached the last page' : `Page ${currentPage + 2}`
          }
          type="button"
        >
          Next
        </button>
      </div>
    </div>
  );
}

export default NavigationBar;
