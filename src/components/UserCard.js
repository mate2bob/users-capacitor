import React from 'react';
import PropTypes from 'prop-types';

function UserCard(props) {
  const { user } = props;
  return (
    <div className="userCard">
      <h2>{user.name}</h2>
      <p>
        <span className="label">Level : </span>
        {user.user_level}
      </p>
      <p>
        <span className="label">Email : </span>
        {user.email}
      </p>
      <div>
        <p>
          <span className="label">Fav :</span>
        </p>
        <ul>
          {user.fav.map((userFav) => (
            <li key={userFav}>{userFav}</li>
          ))}
        </ul>
      </div>
      <div>
        <p>
          <span className="label">Meta : </span>
        </p>
        <p>
          <span className="label">Meta_count :</span> {user.meta.meta_count}
        </p>
        <p>
          <span className="label">Metas : </span>
        </p>
        <ul>
          {user.meta.metas.map((meta) => (
            <li key={meta}>{meta}</li>
          ))}
        </ul>
      </div>
    </div>
  );
}

UserCard.propTypes = {
  user: PropTypes.shape({
    _id: PropTypes.shape({
      $oid: PropTypes.string,
    }),
    name: PropTypes.string,
    user_level: PropTypes.number,
    email: PropTypes.string,
    fav: PropTypes.arrayOf(PropTypes.string),
    meta: PropTypes.shape({
      meta_count: PropTypes.number,
      metas: PropTypes.arrayOf(PropTypes.string),
    }),
  }).isRequired,
};

export default UserCard;
