import axios from 'axios';

const BASE_URL = 'https://ilume-api.herokuapp.com/api/';

const GET_USERS = {
  path: 'users',
  method: 'GET',
};

const GET_USER = {
  path: 'users/',
  method: 'GET',
};

export const getUsers = () => axios[GET_USERS.method.toLowerCase()](`${BASE_URL}${GET_USERS.path}`);

export const getUser = (userId) =>
  axios[GET_USER.method.toLowerCase()](`${BASE_URL}${GET_USER.path}/${userId}`);
